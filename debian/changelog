apiwrap-el (0.5-6) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:37:15 +0900

apiwrap-el (0.5-5) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 20:57:16 +0900

apiwrap-el (0.5-4) unstable; urgency=medium

  * Team upload
  * Rebuild with dh-elpa 2.x

 -- David Bremner <bremner@debian.org>  Fri, 25 Dec 2020 17:10:59 -0400

apiwrap-el (0.5-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sat, 24 Aug 2019 15:50:43 -0300

apiwrap-el (0.5-2) unstable; urgency=medium

  * debian/control:
    - drop Conflicts against emacs24
    - S-V bump 4.1.4 -> 4.2.1 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Wed, 10 Oct 2018 22:18:44 +0200

apiwrap-el (0.5-1) unstable; urgency=medium

  * New upstream release

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 08 Jun 2018 21:49:08 +0200

apiwrap-el (0.4-1) unstable; urgency=medium

  * New upstream release
  * debian/: debhelper bump 10 -> 11
  * debian/control: S-V bump 4.1.1 -> 4.1.3 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 19 Jan 2018 16:43:11 +0100

apiwrap-el (0.3-2) unstable; urgency=medium

  * debian/control: S-V bump 4.0.0 -> 4.1.1 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 20 Oct 2017 15:11:28 +0200

apiwrap-el (0.3-1) unstable; urgency=medium

  * New upstream release
    - debian/patches/: patchset dropped (fixed upstream)

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 20 Oct 2017 14:52:29 +0200

apiwrap-el (0.2.1-2) unstable; urgency=medium

  * debian/patches/: patchset updated
    - 0001-Fix_package_version.patch added

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 08 Sep 2017 23:10:56 +0200

apiwrap-el (0.2.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control: B-D on elpa-dash added

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 01 Sep 2017 21:35:09 +0200

apiwrap-el (0.1.2-2) unstable; urgency=medium

  * Upload to unstable
  * debian/control:
    - S-V bump 3.9.8 -> 4.0.0 (no changes needed)
    - fix dependencies for emacs24
    - drop dependency on emacs-common
  * debian/docs: README.org file added

 -- Matteo F. Vescovi <mfv@debian.org>  Sat, 22 Jul 2017 13:39:53 +0200

apiwrap-el (0.1.2-1) experimental; urgency=medium

  * Initial release (Closes: #864515)

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 09 Jun 2017 21:20:35 +0200
